
import {combineEpics} from 'redux-observable';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/filter';

import { REGISTER_START } from '../store/actions/actionTypes';



function registerEpic(action$: Observable<any>){
    return action$
        .filter (action => action.type === REGISTER_START)
        .do(action => console.log(action))
        .ignoreElements();
}

export const rootEpic: any = combineEpics<any>(registerEpic);