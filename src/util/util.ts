// import { Observable, Observer } from 'rxjs'


// export function createHttpObservable(url: string){
  
//   return new Observable<string>((observer: Observer<string>) => {

//     // Gives us the ability to cancel a request
//     const controller = new AbortController();

//     // If this return true then the request aborts
//     const signal = controller.signal;
    
//     fetch(url, {signal})
      
//       .then(response => {

//         if(response.ok){
//           return response.json();
//         } else {
//           observer.error('Request failed with the status code: ' + response.status);
//         }
        
//       })
      
//       .then(body => {
        
//         observer.next(body)
        
//         observer.complete();
//       })
      
//       // This only handles fatal errors like network issues
//       // It wont catch the error if we return one from our GET request
//       .catch(err =>{
//         observer.error(err);
//       })

//       // Where we will cancel the request
//       return () => controller.abort()
//   });
// }
  