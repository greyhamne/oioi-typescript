//  ============================================================================================================
//  Password Reminder
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as React from 'react';

//  ============================================================================================================
//  Component Dependencies
//  ============================================================================================================
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';

class Register extends React.Component {
    public render(){
        return(
            <React.Fragment>
                <Form>
                    <legend>Title</legend>
                    <Input placeholder="Email Address" />
                    <Button variant="raised">Submit</Button>
                </Form>
            </React.Fragment>
        );
    }
}

export default Register;