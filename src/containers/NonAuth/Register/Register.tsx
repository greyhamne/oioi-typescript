//  ============================================================================================================
//  Register
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index'

//  ============================================================================================================
//  Component Dependencies
//  ============================================================================================================
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';


interface ILocalState {
    registerForm:{
        email: string,
        password: string,
        confirm: string
    },
}

interface IActions {
    register: (email: string, password: string) => void;
}

class Register extends React.Component<IActions> {

    // Local UI State
    public state: ILocalState = {
        registerForm: {
            confirm: '',
            email: '',
            password: ''
        }
    }


    public onChangeHandler = (event: any) => {

        // Create a copy of the current state
        const updatedForm = {
            ...this.state.registerForm
        }

        // Element we want to update
        const updatedElement = event.target.name;
        
        // Add the value we have to the new object containing our state
        updatedForm[updatedElement] = event.target.value;

        // VALIDATION NEEDED

        // update the state
        this.setState({registerForm: updatedForm});
    }

    public onSubmitHandler = (event: any) => {
        // We dont want the form to submit otherwise it refreshes the page
        event.preventDefault();

        this.props.register(this.state.registerForm.email, this.state.registerForm.password);
    }

    public render(){
        return(
            <React.Fragment>
                <Form onSubmit={this.onSubmitHandler.bind(event)}>
                    <legend>Title</legend>
                    <Input 
                        name="email"
                        placeholder="Email Address" 
                        required={true} 
                        onChange={this.onChangeHandler.bind(event)} 
                        value={this.state.registerForm.email}
                        type="email" />

                    <Input name="password" 
                        placeholder="Password" 
                        onChange={this.onChangeHandler.bind(event)} 
                        value={this.state.registerForm.password}/>

                    <Input 
                        name="confirm"
                        placeholder="Confirm Password"  
                        onChange={this.onChangeHandler.bind(event)} 
                        value={this.state.registerForm.confirm}/>

                    <Button variant="raised">Submit</Button>
                </Form>
                <NavLink to="/">login</NavLink>
            </React.Fragment>
        );
    }
}

export function mapDispatchToProps(dispatch: any) {
    return {
        register: (email: string, password: string | number): IActions => dispatch(actions.registerStart(email, password))
    }
} 

export default connect(null, mapDispatchToProps)(Register);