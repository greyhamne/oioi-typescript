//  ============================================================================================================
//  Login & Index
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import {NavLink} from 'react-router-dom'

//  ============================================================================================================
//  UI Dependencies
//  ============================================================================================================
import Form from 'muicss/lib/react/form'; 
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';

interface ILogin {
    onAuth: (email: string, password: string) => void;
}

class Login extends React.Component<ILogin> {

    public render(){

        return(
           <React.Fragment>
                <Form>
                    <legend>Title</legend>
                    <Input placeholder="Email" />
                    <Input placeholder="Password" />
                    <Button variant="raised">Submit</Button>
                </Form>
                <NavLink to="/register">I'm new to oioi</NavLink>
           </React.Fragment>
        )
    }
}

export function mapDispatchToProps(dispatch: any) {
    return {
        onAuth: (email: string, password: string) => dispatch(actions.userAuthenticationStart(email, password))
    }
}
  
export default connect(null, mapDispatchToProps)(Login);