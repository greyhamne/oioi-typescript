//  ============================================================================================================
//  NonAuth Container - mainly handling routes
//  ============================================================================================================
import * as React from 'react';
import { Route } from 'react-router-dom';

//  ============================================================================================================
//  Component Dependencies
//  ============================================================================================================
import Login from './Login/Login';
import Register from './Register/Register';
import PasswordReminder from './PasswordReminder/PasswordReminder';

class NonAuth extends React.Component{
    public render(){
        return(
            <div className="nonAuth">
                <div className="nonAuth__wrapper">
                   
                        <Route exact={true} path="/"  component={Login} />
                        

                        <Route path="/register" component={Register} />
                        <Route path="/password-reminder" component={PasswordReminder} />
                  
                    
                </div>
            </div>
        )
    }
}

export default NonAuth;