//  ============================================================================================================
//  authentication action creators
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as actionTypes from './actionTypes';

//  ============================================================================================================
//  Interfaces - we should create one for every action creator
//  ============================================================================================================
export interface IUserAuthenticationStart {
    type: actionTypes.USER_AUTH_START
}

export interface IUserAuthenticationSuccess {
    type: actionTypes.USER_AUTH_START
}

export interface IUserAuthenticationFail {
    type: actionTypes.USER_AUTH_FAIL
}

// So we know we are going to have 3 actions that belong to the same group 
// Lets assign this to a type so dont have to reference all three
export type  AuthenticationAction =  IUserAuthenticationStart | IUserAuthenticationSuccess | IUserAuthenticationFail;

//  ============================================================================================================
//  Export our action creators including the async action creator - to be created
//  ============================================================================================================

 export const userAuthenticationStart = (email: string, password: string | number) => {
     return {
         type:actionTypes.USER_AUTH_START
     }
 }

 export const UserAuthenticationSuccess = () => {
     return {
         type:actionTypes.USER_AUTH_SUCCESS
     }
 }

 export const UserAuthenticationFail = () => {
     return {
         type: actionTypes.USER_AUTH_FAIL
     }
 }