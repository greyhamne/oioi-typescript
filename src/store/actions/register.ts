//  ============================================================================================================
//  Register
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as actionTypes from './actionTypes';

//  ============================================================================================================
//  Interfaces
//  ============================================================================================================
export interface IRegisterStart {
    type: actionTypes.REGISTER_START
}

export interface IRegisterSuccess {
    type: actionTypes.REGISTER_SUCCESS
}

export interface IRegisterFail {
    type: actionTypes.REGISTER_FAIL
}

// Global export of interfaces

export type RegisterAction = IRegisterStart | IRegisterSuccess | IRegisterFail;

//  ============================================================================================================
//  Action creators
//  ============================================================================================================
export const registerStart = (email: string, password: string | number) => {
    return{
        type: actionTypes.REGISTER_START
    }
}

export const registerSuccess = () => {
    return{
        type: actionTypes.REGISTER_SUCCESS
    }
}

export const registerFail = () => {
    return{
        type: actionTypes.REGISTER_FAIL
    }
}