//  ============================================================================================================
//  ActionTypes.js
//  ============================================================================================================
//  ============================================================================================================
//  Hint: we take advantage of typescript types when creating our action types
//  ============================================================================================================
//  ============================================================================================================
//  User Authentication
//  ============================================================================================================
export const USER_AUTH_START = 'USER_AUTH_START';
export type USER_AUTH_START = typeof USER_AUTH_START;

export const USER_AUTH_SUCCESS = 'USER_AUTH_SUCCESS';
export type USER_AUTH_SUCCESS = typeof USER_AUTH_START;

export const USER_AUTH_FAIL = 'USER_AUTH_FAIL';
export type USER_AUTH_FAIL = typeof USER_AUTH_FAIL;

//  ============================================================================================================
//  New Registration
//  ============================================================================================================
export const REGISTER_START = 'REGISTER_START';
export type REGISTER_START = typeof REGISTER_START

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export type REGISTER_SUCCESS = typeof REGISTER_SUCCESS;

export const REGISTER_FAIL = 'REGISTER_FAIL';
export type REGISTER_FAIL = typeof REGISTER_FAIL;