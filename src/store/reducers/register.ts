//  ============================================================================================================
//  Register reducer
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as actionTypes from '../actions/actionTypes';
import { RegisterAction } from '../actions/register';

interface IRegisterState {
    email: string;
    password: string;
}

const initialState: IRegisterState = {
    email: '',
    password: ''
}

const reducer = (state: IRegisterState = initialState, action: RegisterAction): IRegisterState => {
    switch(action.type){
        case actionTypes.REGISTER_START:
            return {
                ...state
            }

        case actionTypes.REGISTER_FAIL:
        return {
            ...state
        }

        case actionTypes.REGISTER_SUCCESS:
        return {
            ...state 
        }

        
            
    }   
    return state;
}

export default reducer;