//  ============================================================================================================
//  Authentication Reducer
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as actionTypes from '../actions/actionTypes';
import { AuthenticationAction } from '../actions/authentication';

interface IAuthenticationState {
    isUserAuthenticated: boolean;
    // More will be added
}

// Default state
const inititalState: IAuthenticationState = {
    isUserAuthenticated: false
}

// Our storeState is an export of an interface we created
// Key takeaway here is the use of AuthenticationAction holds one value of a possible 3 that were added in /actions/authentication.ts
const reducer = (state: IAuthenticationState = inititalState, action: AuthenticationAction): IAuthenticationState => {
    switch(action.type){
        case actionTypes.USER_AUTH_START:
    }

    return state;
}

export default reducer;