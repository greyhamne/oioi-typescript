//  ============================================================================================================
//  Index
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { applyMiddleware, combineReducers, createStore, compose} from 'redux';

import { Provider } from 'react-redux';

import { createEpicMiddleware } from 'redux-observable';

import { rootEpic } from './epics/index';

const epicMiddleware = createEpicMiddleware();

//  ============================================================================================================
//  Components
//  ============================================================================================================
import App from './App';

//  ============================================================================================================
//  Global Styles
//  ============================================================================================================
import  './assets/style/main.scss'

import registerServiceWorker from './registerServiceWorker';

//  ============================================================================================================
//  Reducers
//  ============================================================================================================
import authentication from './store/reducers/authentication';
import register from './store/reducers/register';

//  ============================================================================================================
//  Creating root reducer
//  ============================================================================================================
const rootReducer  = combineReducers({
  auth: authentication,
  reg: register
});

//  ============================================================================================================
//  Creating a logger
//  ============================================================================================================

declare global {
  interface Window {
    __REDUX__DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

export const store: any = createStore(rootReducer, compose(
  applyMiddleware(epicMiddleware),
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
));

epicMiddleware.run(rootEpic);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
