//  ============================================================================================================
//  Input Component
//  ============================================================================================================
//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import * as React from 'react';

const Inputs = (props: any) => {

    // We are going to be changing the type of input we have so we need a let variable
    let inputElement = null;

    // Default classes that go on inputs
    const defaultClasses = ['form__text'];

    // Default empty Validation Message we will later assign via props
    let validationMsg = null;

    // If the message is invalid and the user has not touch the input
    if (props.invalid && props.touched) {
        validationMsg = <p>Please enter a valid value!</p>;
    }

    // Invalid class
    const invalidClass = 'form__text--inValid';

    // invalid - current status of invalid is true
    // shouldValidate - field is marked as required
    // touched - has the user touched the input
    if(props.invalid && props.shouldValidate && props.touched) {
       // Here we want to add the class to our defaultClasses array
       defaultClasses.push(invalidClass);
    }

    switch (props.elementType){
        case('input'):
            inputElement = <input 
                {...props.elementConfig} 
                value={props.value} 
                onChange={props.changed}
                className = {defaultClasses.join(' ')}
                />;
            break
        case('textarea'):
            inputElement = <textarea {...props.elementConfig}
                {...props.elementConfig} 
                value={props.value} 
                onChange={props.changed}
                className = {defaultClasses.join(' ')} />;
            break
        default:
            inputElement = <input {...props.elementConfig}
            {...props.elementConfig} 
            value={props.value} 
            onChange={props.changed}
            className = {defaultClasses.join(' ')} />;
    }
    
    return(
        <div>
            <label className="c-form__label">{props.label}</label>
            {inputElement}
            {validationMsg}
        </div>
    )
}

export default Inputs;