//  ============================================================================================================
//  Listing model
//  ============================================================================================================
const mongoose = require('mongoose');

// Create our blueprint
const postSchema = mongoose.Schema({
    title: { type: String, required: true },
    content: { type: String, required: true }
});

// Create the model
module.exports = mongoose.model('Listing', postSchema);

