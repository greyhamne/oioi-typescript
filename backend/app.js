//  ============================================================================================================
//  app.js file containing express logic links to server.js in project root
//  ============================================================================================================
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//  ============================================================================================================
//  Models
//  ============================================================================================================
const Listing = require('./models/Listing');

const app = express();

//  ============================================================================================================
//  Connect to our mongodb cluster - this returns a promise
//  ============================================================================================================
mongoose.connect("mongodb+srv://greyhamne:2qgpjDMSRBnYaDqN@cluster0-njo2n.mongodb.net/oioiDatabase?retryWrites=true", {useNewParser: true})
    .then(() => {
        //console.log('Connected to database');
    })
    .catch(() => {
        //console.log('Failed to conect');
    });

app.use(bodyParser.json());

//  ============================================================================================================
//  Allowing CORS
//  ============================================================================================================
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS, PUT");
    next();
});

//  ============================================================================================================
//  Listings
//  ============================================================================================================
app.post('/api/listings', (res, req, next) => {
    const listings = new Listing({
        title: req.body.title,
        /// TS_IGNORE
        content: req.body.content
    });

    listings.save().then(result => {
        // Everything is ok a new resource was created
        res.status(201).json({
            message: "Listing posted successfully",
            listingId: result._id
        })
    });
});

app.get('/api/listings', (res, req, next) => {
    Listing.find().then(documents => {
        res.status(200).json({
            message: "Listing GET request successfull",
            listings: documents
        })
    })
});

app.delete('/api/listings/:id', (res, req, next) => {
    Listing.deleteOne({_id: res.params.id})
        .then(result =>{
            res.status(200).json({message: "Listing Deleted"});
        })
});


// Exporting our app and all the middleware shipped with it
module.exports = app;